program ProjectPokemon;

uses
  Vcl.Forms,
  UnitInicial in 'UnitInicial.pas' {Form3},
  UnitData in 'UnitData.pas' {DataModule4: TDataModule},
  UnitRegistro in 'UnitRegistro.pas' {Form5};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TDataModule4, DataModule4);
  Application.CreateForm(TForm5, Form5);
  Application.Run;
end.
