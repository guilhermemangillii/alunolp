unit UnitInicial;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UnitData, Vcl.DBCtrls, Vcl.StdCtrls,
  Vcl.Imaging.jpeg, Vcl.ExtCtrls;

type
  TForm3 = class(TForm)
    DBLookupListBox1: TDBLookupListBox;
    BtnEd: TButton;
    BtnIns: TButton;
    BtnDel: TButton;
    Image1: TImage;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure BtnDelClick(Sender: TObject);
    procedure BtnInsClick(Sender: TObject);
    procedure BtnEdClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

uses UnitRegistro;

procedure TForm3.BtnDelClick(Sender: TObject);
begin
 DataModule4.FDQuery2.Delete;
end;

procedure TForm3.BtnEdClick(Sender: TObject);
begin
   Form5.Show;
   Form3.Hide;
end;

procedure TForm3.BtnInsClick(Sender: TObject);
begin
   DataModule4.FDQuery2.Append;
   Form5.Show;
   Form3.Hide;
end;

end.
